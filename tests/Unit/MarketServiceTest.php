<?php

namespace Tests\Unit;

use App\Services\MarketService;
use App\User;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\ProductRepository;
use App\Entities\Product;
use Mockery;

class MarketServiceTest extends TestCase
{
    protected $repositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repositoryMock = $this->createMock(ProductRepository::class);
    }

    public function testGetProductList()
    {
        $this->repositoryMock->method('findAll')
            ->will($this->returnCallback(function ()
            {
                return new Collection(self::getProducts());
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $products = $marketServise->getProductList();

        $this->assertInstanceOf(Collection::class, $products);
        $this->assertCount(4, $products);
        $this->assertProductsData($products);
    }

    public function testGetProductById()
    {
        $this->repositoryMock->method('findById')
            ->will($this->returnCallback(function ()
            {
                return self::getProductById(3);
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $product = $marketServise->getProductById(3);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals(3, $product->id);
        $this->assertEquals('product3', $product->name);
        $this->assertEquals(300.22, $product->price);
        $this->assertEquals('1', $product->user_id);
    }

    public function testFailGetProductById()
    {
        $this->repositoryMock->method('findById')
            ->will($this->returnCallback(function ()
            {
                return self::getProductById(7);
            }));

        $marketServise = new MarketService($this->repositoryMock);

        try {
            $marketServise->getProductById(7);
        } catch (\Exception $exception) {
            $this->assertStringContainsString('No product with id: 7', $exception);
        }

    }

    public function testGetProductsByUserId()
    {
        $this->repositoryMock->method('findByUserId')
            ->will($this->returnCallback(function ()
            {
                return new Collection(self::getProductsByUserId(3));
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $products = $marketServise->getProductsByUserId(3);

        $this->assertInstanceOf(Collection::class, $products);
        $this->assertCount(2, $products);
        $this->assertProductsData($products, 3);

    }

    public function testStoreProduct()
    {
        $user = factory(User::class)->make(['id' => 7]);
        $this->be($user);

        $this->repositoryMock->method('store')
            ->will($this->returnCallback(function () use($user)
            {
                return new Product([
                    'id' => 9,
                    'user_id' => auth()->id(),
                    'name' => 'Test Product',
                    'price'    => 999
                ]);
            }));

        $request = $this->createMock(Request::class);

        $requestParams = [
            'product_name' => 'Test Product',
            'product_price'    => 999
        ];

        $request->expects($this->exactly(2))
            ->method('input')
            ->willReturnMap($requestParams);

        $marketServise = new MarketService($this->repositoryMock);

        $product = $marketServise->storeProduct($request);

        $this->assertNotEmpty($product);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals(7, $product->user_id);
        $this->assertEquals('Test Product', $product->name);
        $this->assertEquals(999, $product->price);
        $this->assertEquals(9, $product->id);
    }

    private function assertProductsData(Collection $products, $user_id = null): void
    {
        $products->map(function($product) use ($user_id) {
            $this->assertInstanceOf(Product::class, $product);
            $this->assertNotEmpty($product->id);
            $this->assertNotEmpty($product->name);
            $this->assertNotEmpty($product->user_id);
            if (!is_null($user_id)) {
                $this->assertEquals($user_id, $product->user_id);
            }
            $this->assertIsNumeric($product->price);
        });
    }

    private static function getProductById(int $id): ?Product
    {
        foreach (self::getProducts() as $product) {
            if ($id === $product->id) {
                return $product;
            }
        }

        return null;
    }

    private static function getProductsByUserId(int $id): ?array
    {
        foreach (self::getProducts() as $product) {
            if ($id === $product->user_id) {
                $products[] = $product;
            }
        }

        if (!empty($products)) {
            return $products;
        } else {
            return null;
        }
    }

    private static function getProducts(): array
    {
        return [
                new Product([
                    'id' => 2,
                    'name' => 'product2',
                    'price' => 200.22,
                    'user_id' => 3,
                ]),
                new Product([
                    'id' => 1,
                    'name' => 'product1',
                    'price' => 100.22,
                    'user_id' => 3,
                ]),
                new Product([
                    'id' => 3,
                    'name' => 'product3',
                    'price' => 300.22,
                    'user_id' => 1,
                ]),
                new Product([
                    'id' => 4,
                    'name' => 'product4',
                    'price' => 400.22,
                    'user_id' => 4,
                ])
        ];
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

}
