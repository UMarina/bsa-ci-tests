<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\User;
use App\Entities\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text($maxNbChars = 10),
        'price' => $faker->numberBetween(100, 10000),
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
    ];
});


