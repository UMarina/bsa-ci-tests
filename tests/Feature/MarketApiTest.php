<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Entities\Product;
use Tests\TestDataFactory;

class MarketApiTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        factory(User::class, 5)->create()
            ->each(function ($user) {
                $user->products()->saveMany(
                    factory(Product::class, 4)->create(['user_id' => $user->id])
                );
            });
    }

    public function testShowList()
    {
        $data = $this->json('get', 'api/items')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'price',
                    'user_id'
                ]
            ])
            ->assertJsonCount(20)
            ->json();

        $this->assertNotEmpty($data);

        foreach ($data as $item) {
            $this->assertProductData($item);
        }
    }

    public function testShowProduct()
    {
        $user = User::inRandomOrder()->first();
        $product = Product::where('user_id', $user->id)->first();

        $data = $this->json('get', "api/items/{$product->id}")
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'price',
                'user_id'
            ])
            ->json();

        $this->assertNotEmpty($data);
        $this->assertProductData($data);
    }

    public function testFailedShowProduct()
    {
        Product::destroy(199);

        $data = $this->json('get', 'api/items/199')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->assertJsonStructure([
                'message',
            ])
            ->json();

        $this->assertEquals('product not found', $data['message']);
    }

    public function testStore()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');

        $dataProduct = [
            'product_name' => 'Test',
            'product_price' => 345,
        ];

        $data = $this->json('post', "api/items/", $dataProduct)
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'name',
                'price',
                'user_id'
            ])->json();

        $this->assertDatabaseHas('products', [
            'id' => $data['id'],
            'name' => 'Test',
            'price' => 345,
            'user_id' => $user->id,
        ]);
    }

    public function testNotAuthStore()
    {
        $dataProduct = [
            'product_name' => 'Test',
            'product_price' => 345,
        ];

        $this->json('post', "api/items/", $dataProduct)
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(401)
            ->json();

    }

    public function testDelete()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');
        $product = Product::where('user_id', $user->id)->first();

        $this->json('delete', "api/items/{$product->id}")
            ->assertStatus(204);

        $this->assertDatabaseMissing('products', ['id' => $product->id]);
    }

    public function testFailedDelete()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');
        Product::destroy(199);

        $data = $this->json('delete', 'api/items/199')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->json();

        $this->assertEquals('product not found', $data['message']);
    }

    public function testForbiddenDelete()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');
        $product = Product::whereNotIn('user_id', [$user->id])->first();

        $data = $this->json('delete', "api/items/{$product->id}")
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ])
            ->json();

        $this->assertEquals('Forbidden', $data['message']);
    }

    private function assertProductData($product, $user_id = null): void
    {
            $this->assertNotEmpty($product['id']);
            $this->assertIsString($product['name']);
            $this->assertNotEmpty($product['user_id']);
            if (!is_null($user_id)) {
                $this->assertEquals($user_id, $product['user_id']);
            }
            $this->assertIsString($product['price']);
    }
}
