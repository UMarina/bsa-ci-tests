<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MarketService;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList()
    {
        $products = $this->marketService->getProductList();

        return new Response(
            $products->map(function ($product){
                return new ProductResource($product);
            }), 200
        );
    }

    public function store(Request $request)
    {
        $product = $this->marketService->storeProduct($request);

        return new Response(new ProductResource($product), 201);

    }

    public function showProduct(int $id)
    {
        try {
            $product = $this->marketService->getProductById($id);
        } catch (\Exception $e) {
            return new Response([
                'message' => 'product not found'
            ], 400);
        }
        return new Response(new ProductResource($product), 200);

    }

    public function delete(Request $request)
    {
        try {
            $resultDelete = $this->marketService->deleteProduct($request);

            if($resultDelete) {
                return new Response([], 204);
            }

            return new Response([
                'message' => 'Forbidden'
            ], 403);

        } catch (\Exception $e) {
            return new Response([
                'message' => 'product not found'
            ], 400);
        }
    }
}
